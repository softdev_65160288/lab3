package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */



/**
 *
 * @author informatics
 */
public class Lab3 {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
    static boolean checkWin(String table[][], String currentPlayer){
        if(checkCol(table,currentPlayer)){
            return true;
        }else if(checkRow(table,currentPlayer)){
            return true;
        }else if(checkDia(table,currentPlayer)){
            return true;
        }
        return false;
    }
    static boolean Draw(String table[][]){
        if(checkDraw(table)){
            return true;
        }
        return false;
    }
    
    private static boolean checkCol(String[][] table, String currentPlayer){
        for(int row = 0;row<3;row++){
            if(table[row][0].equals(currentPlayer)){
                return true;
            }
        }
        for(int row = 0;row<3;row++){
            if(table[row][1].equals(currentPlayer)){
                return true;
            }
        }for(int row = 0;row<3;row++){
            if(table[row][2].equals(currentPlayer)){
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer){
        for(int col = 0;col<3;col++){
            if(table[0][col].equals(currentPlayer)){
                return true;
            }
        }
        for(int col = 0;col<3;col++){
            if(table[1][col].equals(currentPlayer)){
                return true;
            }
        }for(int col = 0;col<3;col++){
            if(table[2][col].equals(currentPlayer)){
                return true;
            }
        }
        return false;
    }
    private static boolean checkDia(String[][] table, String currentPlayer){
        for(int row = 0,col = 0;row < 3;row++,col++){
            if(table[row][col].equals(currentPlayer)){
                return true;
            }
        }
        for(int row = 0,col = 3;row < 3;row++,col--){
            if(table[row][col].equals(currentPlayer)){
                return true;
            }
        }return false;
    }
    private static boolean checkDraw(String[][] table){
        for(int row = 0;row < 3;row++){
            for(int col = 0;col < 3;col++){
                if(table[row][col].equals("X") ||table[row][col].equals("O")){
                    return true;
                }
            }
        }
        return false;
    }
}
